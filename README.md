# Conference GeekSpace: Docker

## Run Docker Image `extra-info`

```
docker load < (path)/extra-info.tar
```
```
docker run extra-info 
```

## Useful links

### Isolating and running of container in Linux Kernel
* https://www.nginx.com/blog/what-are-namespaces-cgroups-how-do-they-work/
* https://www.toptal.com/linux/separation-anxiety-isolating-your-system-with-linux-namespaces
* Deep info about linux namesapces
  - https://habr.com/ru/post/458462/
  - https://habr.com/ru/post/459574/
  - https://habr.com/ru/post/541304/
  - https://habr.com/ru/post/549414/
* https://medium.com/@teddyking/linux-namespaces-850489d3ccf
* https://www.redhat.com/sysadmin/7-linux-namespaces
* https://www.redhat.com/sysadmin/mount-namespaces
* https://opensource.com/article/19/10/namespaces-and-containers-linux
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/global_file_system_2/s1-manage-mountorder

### What is Ubuntu Images
* https://partner-images.canonical.com/core/
* https://partner-images.canonical.com/core/focal/

### Docker CLI
* https://docs.docker.com/engine/reference/run/

### General Info
* Learning docker
  - https://habr.com/ru/company/ruvds/blog/438796/
  - https://habr.com/ru/company/ruvds/blog/439978/
  - https://habr.com/ru/company/ruvds/blog/439980/
  - https://habr.com/ru/company/ruvds/blog/440658/
  - https://habr.com/ru/company/ruvds/blog/440660/
  - https://habr.com/ru/company/ruvds/blog/441574/
* https://habr.com/ru/post/253877/
* https://habr.com/ru/company/southbridge/blog/515508/
* https://dhathriblog.medium.com/difference-between-docker-image-and-container-85354526c914
* https://blog.ithillel.ua/articles/chto-takoe-docker-prostymi-slovami-o-konteynerizatsii
* https://mcs.mail.ru/blog/chto-takoe-docker-i-kak-on-rabotaet
* https://losst.ru/montirovanie-diska-v-linux
* https://ru.wikipedia.org/wiki/POSIX
* https://losst.ru/chto-takoe-posix

### Videos
* https://youtu.be/QF4ZF857m44
* https://youtu.be/sK5i-N34im8
* https://www.youtube.com/playlist?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/v_GbcTpMTLE?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/6XlqSfOEiGU?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/wVSQ0_v3t_8?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/C4w0Kk52EcA?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/8a3gDy6ykHE?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
  - https://youtu.be/6fczsvaKxp0?list=PLvTBThJr861x2qFBVwOlqIrbaft-Im-0u
